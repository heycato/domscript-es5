window.domscript = {}
;(function(win, doc, ns) {
///////////////////
// BEGIN UTIL.JS //
///////////////////
  var clone = function(obj) {
    var output
    try { output = JSON.parse(JSON.stringify(obj)) }
    catch(e) { console.error(e) }
    return output
  }

  var getType = function(obj) {
    return Object.prototype.toString.apply(obj)
      .replace('[object ', '')
      .replace(']', '')
      .toLowerCase()
  }

  ns.util = {
    clone:clone,
    getType:getType
  }
///////////////////
// END UTIL.JS //
///////////////////



//////////////////
// BEGIN DOM.JS //
//////////////////
  var tags = [ "a", "abbr", "acronym", "address", "applet", "area", "article", "aside", "audio", "b", "base", "basefont", "bdi", "bdo", "big", "blockquote", "body", "br", "button", "canvas", "caption", "center", "cite", "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog", "dir", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "font", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hr", "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li", "link", "main", "map", "mark", "menu", "menuitem", "meta", "meter", "nav", "noframes", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strike", "strong", "style", "sub", "summary", "sup", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "tt", "u", "ul", "var", "video"]
  var attributeExceptions = ['role', 'attribute']

  var childAppender = function(win, doc, parent, child) {
    if(Array.isArray(child)) {
      child.reduce(childAppender.bind(null, win, doc), parent)
    } else if(child instanceof win.Element) {
      parent.appendChild(child)
    } else if(typeof child === 'string') {
      parent.appendChild(doc.createTextNode(child))
    }
    return parent
  }

  var setStyles = function(el, styles) {
    if(!styles) {
      el.removeAttribute('styles')
      return el
    }
    Object.keys(styles)
      .map(function(prop) {
        if(prop in el.style) {
          el.style[prop] = styles[prop]
        } else {
          console.warn(styleName + " is not a valid style for a <" + el.tagName.toLowerCase() + ">");
        } 
      })
    return el
  }

  var setAttributes = function(el, attrs) {
    Object.keys(attrs)
      .map(function(prop) {
        if(attrs[prop] === undefined) {
          el.removeAttribute(prop)
        } else {
          el.setAttribute(prop, attrs[prop])
        }
      })
    return el
  }

  var createElement = function() {
    var args = [].slice.apply(arguments)
    var win = args[0]
    var doc = args[1]
    var type = args[2]
    var textOrPropsOrChild = args[3]
    var otherChildren = args.slice(3, args.length)
    var el = doc.createElement(type)
    var appendChild = childAppender.bind(null, win, doc)

    if(Object.prototype.toString.apply(textOrPropsOrChild) === '[object Object]') {
      Object.keys(textOrPropsOrChild)    
        .map(function(name) {
          if(name in el || attributeExceptions.indexOf(name) > -1) {
            const value = textOrPropsOrChild[name]
            if(name === "style") {
              setStyles(el, value)
            } else if(name === "attribute") {
              setAttributes(el, value)
            } else if(value) {
              el[name] = value
            }
          } else {
            console.warn(name+ " is not a valid property of a <" + type + ">")
          }
        })
    } else {
      appendChild(el, textOrPropsOrChild)
    }
    
    if(otherChildren) appendChild(el, otherChildren)
    return el
  }

  ns.dom = tags.reduce(function(acc, tag) {
      acc[tag] = createElement.bind(null, win, doc, tag)
      return acc
    }, {})
////////////////
// END DOM.JS //
////////////////



////////////////////////
// BEGIN COMPONENT.JS //
////////////////////////
  ns.createComponent = function(msgr, name, render, initData, noUpdate) {

    var el = render(initData)

    var update = function(prevEl, newData) {
      var nextEl = render(newData)
      if(!nextEl.isEqualNode(prevEl)) {
        prevEl.parentElement.replaceChild(nextEl, prevEl)
        return nextEl
      } else {
        msgr.emit('warn', {
          message: "render() called but no changed detected for: " + name
        })
        return prevEl
      }
    }
    
    if(!noUpdate) {
      msgr.on('update', function(data) {
        msgr.emit('info', {
          message: "Updating: " + name + " with " + JSON.stringify(data)
        })
        el = update(el, data)
      })
    }

    return el
  }
//////////////////////
// END COMPONENT.JS //
//////////////////////



////////////////////
// BEGIN STORE.JS //
////////////////////
  ns.createStore = function(model) {
    model = model || {}
    return {
      getState: function() { return clone(model) }, 
      get: function(path) {
        return clone(
          path.split('.')
            .reduce(function(acc, key) {
              return acc ? acc[key] : undefined
            }, model)
        )
      },
      set: function(value, path) {
        var pathArr = path.split('.')
        var len = pathArr.length
        return clone(pathArr.reduce(function(acc, key, i) {
          if(acc && typeof acc[key] !== 'object') acc[key] = {}
          if(acc && i === len - 1) acc[key] = value
          return acc[key]
        }, model))
      },
      del: function(path) {
        var pathArr = path.split('.')
        var len = pathArr.length
        return pathArr.reduce(function(acc, key, i) {
          if(acc && i === len - 1) delete acc[key]
          return acc[key]
        }, model)
      }
    }
  }
//////////////////
// END STORE.JS //
//////////////////



////////////////////////
// BEGIN MESSENGER.JS //
////////////////////////
  function Messenger() {
    this.messages = []
  }

  Messenger.prototype = {
    on: function(type, cb) {
      this.messages.push({type:type, cb:cb})
    },
    off: function(type, cb) {
      this.messages = this.messages
        .filter(function(msg) { 
          return msg.type.toString() !== type.toString() ||
            msg.cb.toString() !== cb.toString()
        })
    },
    emit: function(type, data) {
      this.messages
        .forEach(function(msg) {
          if(getType(msg.type) === 'regexp') {
            if(msg.type.test(type)) {
              msg.cb.apply(null, [payload])
            }
          } else if(msg.type.toString() === type.toString()) {
            msg.cb.apply(null, [payload])
          }
        })
    }
  }
  
  ns.createMessenger = function() { return new Messenger() }
//////////////////////
// END MESSENGER.JS //
//////////////////////

}(window, document, domscript))
